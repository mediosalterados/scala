<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PostViewController extends AbstractController {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/noticia/{url}", name="post_view")
     */
    public function reedem(News $news) {
        $date = $news->getFecha();
        $category = $this->entityManager->getRepository(Category::class)->findAll();
        $next = $this->entityManager->getRepository(News::class)->findByNext($date);
        $previous = $this->entityManager->getRepository(News::class)->findByPrevious($date);
        $posts = $this->entityManager->getRepository(News::class)->findBy(
                array(), array('fecha' => 'DESC'), 3
        );

        return $this->render('post_view/index.html.twig', [
                    'controller_name' => 'PostViewController',
                    "post" => $news,
                    "posts" => $posts,
                    "previous" => $previous,
                    "next" => $next,
                    "category" => $category,
        ]);
    }

    /**
     * @Route("/categoria/{url}", name="category_view")
     */
    public function category(Category $category) {


        return $this->render('blog/category.html.twig', [
                    'name' => $category,
                    "posts" => $category->getNew(),
        ]);
    }

}
