<?php

namespace App\Controller\Admin;

use App\Entity\Coffee;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;


class CoffeeCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Coffee::class;
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud
                        ->setPageTitle('index', 'Cafe Scala')
                        ->setPageTitle('edit', 'Editar cafe')
                        ->setPageTitle('new', 'Nuevo cafe');
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('name', 'Nombre'),
        TextField::new('title_esp', 'Título Esp'),
        TextField::new('title_eng', 'Título Eng'),
        TextEDitorField::new('description_esp', 'Descripción Esp'),
        TextEDitorField::new('description_eng', 'Descripción Eng'),
        TextField::new('code_esp', 'Codigo de video Esp'),
        TextField::new('code_eng', 'Codigo de video Eng'),
        DateField::new('date', 'Fecha')->setFormat('Y-MM-dd')->renderAsChoice()->onlyOnForms(),
        AssociationField::new('categories', 'Categoria')->onlyOnForms(),
        ];
    }

}
