<?php

namespace App\Controller\Admin;

use App\Entity\Testimony;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class TestimonyCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Testimony::class;
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud
                        ->setPageTitle('index', 'Testimonios')
                        ->setPageTitle('edit', 'Editar Testimonios')
                        ->setPageTitle('new', 'Nuevo Testimonios');
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('name', 'Nombre'),
        TextareaField::new('photoFile', 'Foto')->setFormType(VichImageType::class)->onlyOnForms(),
        TextField::new('title_esp', 'Titulo Esp'),
        TextField::new('title_eng', 'Titulo Eng'),
        TextEDitorField::new('description_esp', 'Descripción Esp'),
        TextEDitorField::new('description_eng', 'Descripción Eng'),
        Field::new('age', 'Edad'),
        ];
    }

}
