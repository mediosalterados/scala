<?php

namespace App\Controller\Admin;

use App\Entity\PlayBook;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class PlayBookCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return PlayBook::class;
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud
                        ->setPageTitle('index', 'Play Book')
                        ->setPageTitle('edit', 'Editar Play Book')
                        ->setPageTitle('new', 'Nuevo Play Book');
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('nombre_esp', 'Nombre Esp'),
        TextField::new('nombre_eng', 'Nombre Eng'),
        TextField::new('url'),
        TextareaField::new('imageFile', 'Imagen')->setFormType(VichImageType::class)->onlyOnForms(),
        ];
    }

}
