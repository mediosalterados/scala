<?php

namespace App\Controller\Admin;

use App\Entity\Team;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;


class TeamCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Team::class;
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud
                        ->setPageTitle('index', 'Equipo')
                        ->setPageTitle('edit', 'Editar Equipo')
                        ->setPageTitle('new', 'Nuevo Equipo');
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('name', 'Nombre'),
        TextField::new('last_name', 'Apellidos'),
        TextareaField::new('photoFile', 'Foto')->setFormType(VichImageType::class)->onlyOnForms(),
        TextEDitorField::new('description_esp', 'Descripción Esp'),
        TextEDitorField::new('description_eng', 'Descripción Eng'),
        TextField::new('position', 'Puesto Esp'),
        TextField::new('position_eng', 'Puesto Eng'),
        TextField::new('url', 'url'),
        ];
    }

}
