<?php

namespace App\Controller\Admin;

use App\Entity\Category;
use App\Entity\News;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class CategoryCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Category::class;
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud
                        ->setPageTitle('index', 'Categorias')
                        ->setPageTitle('edit', 'Editar Categoria')
                        ->setPageTitle('new', 'Nuevo Categoria');
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('name', 'Nombre Esp '),
        TextField::new('nameEsp', 'Nombre Eng'),
        TextField::new('color', 'Color'),
        TextField::new('url'),
        ];
    }

}
