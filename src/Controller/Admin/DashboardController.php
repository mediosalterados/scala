<?php

namespace App\Controller\Admin;

use App\Entity\News;
use App\Entity\Category;
use App\Entity\Team;
use App\Entity\Testimony;
use App\Entity\Contact;
use App\Entity\Coffee;
use App\Entity\Ally;
use App\Entity\PlayBook;
use App\Entity\Linkedin;
use EasyCorp\Bundle\EasyAdminBundle\Config\Dashboard;
use EasyCorp\Bundle\EasyAdminBundle\Config\MenuItem;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractDashboardController;
use EasyCorp\Bundle\EasyAdminBundle\Router\CrudUrlGenerator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DashboardController extends AbstractDashboardController
{
    /**
     * @Route("/admin", name="admin")
     * 
     * @return Response
     */
    public function index(): Response
    {
        
         $routeBuilder = $this->get(CrudUrlGenerator::class)->build();

//        return $this->redirect($routeBuilder->setController(CycleCrudController::class)->generateUrl());
        
        return parent::index();
    }

    public function configureDashboard(): Dashboard
    {
        return Dashboard::new()
            ->setTitle('Scala');
    }

    public function configureMenuItems(): iterable
    {
        yield MenuItem::linktoDashboard('Dashboard', 'fa fa-home');
        yield MenuItem::linkToCrud('Notas', 'far fa-newspaper', News::class);
        yield MenuItem::linkToCrud('Categorias', 'fas fa-archive', Category::class);
        yield MenuItem::linkToCrud('Equipo', 'fas fa-users', Team::class);
        yield MenuItem::linkToCrud('Testimonios', 'fas fa-comments', Testimony::class);
        yield MenuItem::linkToCrud('contactos', 'fas fa-address-book', Contact::class);
        yield MenuItem::linkToCrud('Cafe', 'fas fa-mug-hot', Coffee::class);
        yield MenuItem::linkToCrud('Linkedin', 'fab fa-linkedin', Linkedin::class);
        yield MenuItem::linkToCrud('Aliados', 'fas fa-globe', Ally::class);
        yield MenuItem::linkToCrud('Play Book', 'fab fa-discourse', PlayBook::class);
    }
}

