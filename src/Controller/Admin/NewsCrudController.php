<?php

namespace App\Controller\Admin;

use App\Entity\News;
use App\Entity\Category;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateField;
use EasyCorp\Bundle\EasyAdminBundle\Field\Field;

class NewsCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return News::class;
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud->addFormTheme('@FOSCKEditor/Form/ckeditor_widget.html.twig')
                        ->setPageTitle('index', 'Notas')
                        ->setPageTitle('edit', 'Editar Nota')
                        ->setPageTitle('new', 'Nuevo Nota');
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('name', 'Nombre'),
        TextareaField::new('imageFile', 'Imagen')->setFormType(VichImageType::class)->onlyOnForms(),
        TextareaField::new('bannerFile', 'Banner')->setFormType(VichImageType::class)->onlyOnForms(),
        TextField::new('titulo_esp', 'Título Esp'),
        TextField::new('titulo_eng', 'Título Eng'),
        TextareaField::new('contenido_esp', 'Contenido Esp')->setFormType(CKEditorType::class)->onlyOnForms(),
        TextEditorField::new('contenido_eng', 'Contenido Eng')->setFormType(CKEditorType::class)->onlyOnForms(),
        AssociationField::new('categories', 'Categoría')->onlyOnForms(),
        DateField::new('fecha', 'Fecha')->setFormat('Y-MM-dd')->renderAsChoice()->onlyOnForms(),
        Field::new('description', 'Descripción Esp'),
        Field::new('description_eng', 'Descripción Eng'),
        Field::new('destacado', 'Destacado'),
        Field::new('url'),
        ];
    }

}
