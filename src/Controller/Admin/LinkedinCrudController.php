<?php

namespace App\Controller\Admin;

use App\Entity\Linkedin;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class LinkedinCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Linkedin::class;
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud
                        ->setPageTitle('index', 'Linkedin')
                        ->setPageTitle('edit', 'Editar Linkedin')
                        ->setPageTitle('new', 'Nuevo Linkedin');
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('name', 'Nombre'),
        TextareaField::new('code', 'codigo'),
        ];
    }

}
