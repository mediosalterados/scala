<?php

namespace App\Controller\Admin;

use App\Entity\Contact;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextEditorField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class ContactCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Contact::class;
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud
                        ->setPageTitle('index', 'Contactos')
                        ->setPageTitle('edit', 'Editar Contacto')
                        ->setPageTitle('new', 'Nuevo Contacto');
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('name', 'Nombre'),
        TextField::new('mail', 'Correo'),
        TextField::new('position', 'Puesto'),
        TextField::new('institution', 'Institución'),
        TextField::new('Country', 'País'),
        TextEditorField::new('message', 'Mensaje'),
        ];
    }

}
