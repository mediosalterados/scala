<?php

namespace App\Controller\Admin;

use App\Entity\Ally;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;

class AllyCrudController extends AbstractCrudController {

    public static function getEntityFqcn(): string {
        return Ally::class;
    }

    public function configureCrud(Crud $crud): Crud {
        return $crud
                        ->setPageTitle('index', 'Aliados')
                        ->setPageTitle('edit', 'Editar aliado')
                        ->setPageTitle('new', 'Nuevo aliado');
    }

    public function configureFields(string $pageName): iterable {
        return [
        TextField::new('name', 'Nombre'),
        TextareaField::new('imageFile', 'Logotipo')->setFormType(VichImageType::class)->onlyOnForms(),
        ];
    }

}
