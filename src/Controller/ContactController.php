<?php

namespace App\Controller;

use App\Form\ContactType;
use App\Entity\Contact;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;

class ContactController extends AbstractController {

    /**
     * @Route("/formulario", name="contact")
     */
    public function index(Request $request, MailerInterface $mailer) {


        $contact = new Contact();
        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($contact);
            $entityManager->flush();

            $email = (new TemplatedEmail())
                    ->from(new Address('intranet@silanes.com.mx', 'coRREo '))
                    ->to('diana.boggio@scalahed.com')
                    ->bcc('especie360@hotmail.com')
                    ->subject('Gracias por llenar el test test ')
                    ->htmlTemplate('mail/index.html.twig')
                    ->context([
                'user' => $contact,
                    ])
            ;

            $mailer->send($email);

            return $this->redirectToRoute('gracias');
        }
        return $this->render('contact/index.html.twig', [
                    'controller_name' => 'ContactController',
                    'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/gracias", name="gracias")
     */
    public function gracias(): Response {
        return $this->render('contact/gracias.html.twig', [
                    'controller_name' => 'ScalaModelController',
        ]);
    }

    /**
     * @Route("/contactanos", name="contactanos")
     */
    public function contactanos(): Response {
        return $this->render('contact/contacto.html.twig', [
                    'controller_name' => 'ScalaModelController',
        ]);
    }

}
