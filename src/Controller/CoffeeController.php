<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Coffee;
use App\Entity\Category;

class CoffeeController extends AbstractController {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/cafe", name="coffee")
     */
    public function index(): Response {

        $posts = $this->entityManager->getRepository(Coffee::class)->findBy(
                array(), array('date' => 'DESC')
        );


        return $this->render('coffee/index.html.twig', [
                    'controller_name' => 'CoffeeController',
                    "posts" => $posts,
        ]);
    }

}
