<?php

namespace App\Controller;

use App\Entity\Testimony;
use App\Entity\Ally;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class AlliesController extends AbstractController {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/aliados", name="allies")
     */
    public function index(): Response {
        $testimonials = $this->entityManager->getRepository(Testimony::class)->findAll();

        $allies = $this->entityManager->getRepository(Ally::class)->findAll();

        return $this->render('allies/index.html.twig', [
                    'controller_name' => 'AlliesController',
                    'testimonials' => $testimonials,
                    'allies' => $allies
        ]);
    }

}
