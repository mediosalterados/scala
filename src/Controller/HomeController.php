<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\Linkedin;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/", name="home")
     */
    public function index(): Response {

        $posts = $this->entityManager->getRepository(News::class)->findBy(array('destacado' => 1), array('fecha' => 'DESC'));
        $linkedin = $this->entityManager->getRepository(Linkedin::class)->findAll();
        
        return $this->render('home/index.html.twig', [
                    'controller_name' => 'HomeController',
                    "posts" => $posts,
                    "linkedin" => $linkedin
        ]);
    }

}
