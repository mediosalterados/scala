<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ScalaModelController extends AbstractController
{
    /**
     * @Route("/modelo-scala", name="scala_model")
     */
    public function index(): Response
    {
        return $this->render('scala_model/index.html.twig', [
            'controller_name' => 'ScalaModelController',
        ]);
    }
}
