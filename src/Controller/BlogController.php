<?php

namespace App\Controller;

use App\Entity\News;
use App\Entity\Coffee;
use App\Entity\PlayBook;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController {

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/noticias", name="blog")
     */
    public function index(): Response {
        $posts = $this->entityManager->getRepository(News::class)->findAll();
        $coffee = $this->entityManager->getRepository(Coffee::class)->findAll();
        $playBook = $this->entityManager->getRepository(PlayBook::class)->findAll();

        return $this->render('blog/index.html.twig', [
                    'controller_name' => 'BlogController',
                    "posts" => $posts,
                    "coffee" => $coffee,
                    "playBook" => $playBook,
        ]);
    }

    /**
     * @Route("/play/{id}", name="play_view")
     */
    public function play(PlayBook $playBook) {

        return $this->render('blog/play.html.twig', [
                    "video" => $playBook,
                    
        ]);
    }

}
