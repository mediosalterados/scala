<?php

namespace App\Controller;

use App\Entity\Team;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Doctrine\ORM\EntityManagerInterface;

class AboutUsController extends AbstractController
{
    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    public function __construct(string $name = null, EntityManagerInterface $entityManager) {
        $this->entityManager = $entityManager;
    }

    /**
     * @Route("/sobre-nosotros", name="about_us")
     */
    public function index(): Response
    {
        $team = $this->entityManager->getRepository(Team::class)->findBy(array(), array('last_name' => 'ASC'));

        return $this->render('about_us/index.html.twig', [
            'controller_name' => 'AboutUsController',
            'team' => $team
        ]);
    }
}
