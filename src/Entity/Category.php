<?php

namespace App\Entity;

use App\Repository\CategoryRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CategoryRepository::class)
 */
class Category
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=News::class, mappedBy="categories")
     * 
     */
    private $new;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $color;

    /**
     * @ORM\ManyToMany(targetEntity=Coffee::class, mappedBy="categories")
     */
    private $coffee;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $nameEsp;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $url;

    public function __construct()
    {
        $this->new = new ArrayCollection();
        $this->coffee = new ArrayCollection();
    }
    
    public function __toString() {
        return $this->name;
    }
    


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|News[]
     */
    public function getNew(): Collection
    {
        return $this->new;
    }

    public function addNew(News $new): self
    {
        if (!$this->new->contains($new)) {
            $this->new[] = $new;
        }

        return $this;
    }

    public function removeNew(News $new): self
    {
        if ($this->new->contains($new)) {
            $this->new->removeElement($new);
        }
        


        return $this;
    }

    public function getColor(): ?string
    {
        return $this->color;
    }

    public function setColor(?string $color): self
    {
        $this->color = $color;

        return $this;
    }

    /**
     * @return Collection|Coffee[]
     */
    public function getCoffee(): Collection
    {
        return $this->coffee;
    }

    public function addCoffee(Coffee $coffee): self
    {
        if (!$this->coffee->contains($coffee)) {
            $this->coffee[] = $coffee;
            $coffee->addCategory($this);
        }

        return $this;
    }

    public function removeCoffee(Coffee $coffee): self
    {
        if ($this->coffee->removeElement($coffee)) {
            $coffee->removeCategory($this);
        }

        return $this;
    }

    public function getNameEsp(): ?string
    {
        return $this->nameEsp;
    }

    public function setNameEsp(?string $nameEsp): self
    {
        $this->nameEsp = $nameEsp;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }


}
