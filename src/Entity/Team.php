<?php

namespace App\Entity;

use App\Repository\TeamRepository;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TeamRepository::class)
 * @Vich\Uploadable
 */
class Team
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $description_eng;

    /**
     * @ORM\Column(type="text")
     */
    private $description_esp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $position;
    
    
     /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @Vich\UploadableField(mapping="new_images", fileNameProperty="photo")
     * @var File
     */
    private $photoFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $url;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $positionEng;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $last_name;
    
    
    
     /**
     * @return mixed
     */
    public function getPhotoFile()
    {
        return $this->photoFile;
    }

    /**
     * @param mixed $imageFile
     */
    public function setPhotoFile($photoFile): void
    {
        $this->photoFile = $photoFile;

        if ($photoFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    public function getPhoto()
    {
        return $this->photo;
    }
    

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescriptionEng(): ?string
    {
        return $this->description_eng;
    }

    public function setDescriptionEng(string $description_eng): self
    {
        $this->description_eng = $description_eng;

        return $this;
    }

    public function getDescriptionEsp(): ?string
    {
        return $this->description_esp;
    }

    public function setDescriptionEsp(string $description_esp): self
    {
        $this->description_esp = $description_esp;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(?string $url): self
    {
        $this->url = $url;

        return $this;
    }

    public function getPositionEng(): ?string
    {
        return $this->positionEng;
    }

    public function setPositionEng(?string $positionEng): self
    {
        $this->positionEng = $positionEng;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->last_name;
    }

    public function setLastName(string $last_name): self
    {
        $this->last_name = $last_name;

        return $this;
    }
}
