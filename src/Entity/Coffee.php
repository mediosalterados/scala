<?php

namespace App\Entity;

use App\Repository\CoffeeRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=CoffeeRepository::class)
 */
class Coffee
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class, inversedBy="title_esp")
     */
    private $categories;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title_esp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title_eng;

    /**
     * @ORM\Column(type="text")
     */
    private $description_esp;

    /**
     * @ORM\Column(type="text")
     */
    private $description_eng;

    /**
     * @ORM\Column(type="date")
     */
    private $date;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code_esp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $code_eng;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        $this->categories->removeElement($category);

        return $this;
    }

    public function getTitleEsp(): ?string
    {
        return $this->title_esp;
    }

    public function setTitleEsp(string $title_esp): self
    {
        $this->title_esp = $title_esp;

        return $this;
    }

    public function getTitleEng(): ?string
    {
        return $this->title_eng;
    }

    public function setTitleEng(string $title_eng): self
    {
        $this->title_eng = $title_eng;

        return $this;
    }

    public function getDescriptionEsp(): ?string
    {
        return $this->description_esp;
    }

    public function setDescriptionEsp(string $description_esp): self
    {
        $this->description_esp = $description_esp;

        return $this;
    }

    public function getDescriptionEng(): ?string
    {
        return $this->description_eng;
    }

    public function setDescriptionEng(string $description_eng): self
    {
        $this->description_eng = $description_eng;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getCodeEsp(): ?string
    {
        return $this->code_esp;
    }

    public function setCodeEsp(string $code_esp): self
    {
        $this->code_esp = $code_esp;

        return $this;
    }

    public function getCodeEng(): ?string
    {
        return $this->code_eng;
    }

    public function setCodeEng(string $code_eng): self
    {
        $this->code_eng = $code_eng;

        return $this;
    }
}
