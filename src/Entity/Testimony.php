<?php

namespace App\Entity;

use App\Repository\TestimonyRepository;
use Symfony\Component\HttpFoundation\File\File;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TestimonyRepository::class)
 * @Vich\Uploadable
 */
class Testimony
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\Column(type="text")
     */
    private $title_esp;

    /**
     * @ORM\Column(type="text")
     */
    private $title_eng;

    /**
     * @ORM\Column(type="text")
     */
    private $description_esp;

    /**
     * @ORM\Column(type="text")
     */
    private $description_eng;
    
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $photo;

    /**
     * @Vich\UploadableField(mapping="testimony_images", fileNameProperty="photo")
     * @var File
     */
    private $photoFile;
       
    /**
     * @return mixed
     */
    public function getPhotoFile()
    {
        return $this->photoFile;
    }

    /**
     * @param mixed $imageFile
     */
    public function setPhotoFile($photoFile): void
    {
        $this->photoFile = $photoFile;

        if ($photoFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function setPhoto($photo)
    {
        $this->photo = $photo;
    }

    public function getPhoto()
    {
        return $this->photo;
    }
    
    

    /**
     * @ORM\Column(type="integer")
     */
    private $age;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getTitleEsp(): ?string
    {
        return $this->title_esp;
    }

    public function setTitleEsp(string $title_esp): self
    {
        $this->title_esp = $title_esp;

        return $this;
    }

    public function getTitleEng(): ?string
    {
        return $this->title_eng;
    }

    public function setTitleEng(string $title_eng): self
    {
        $this->title_eng = $title_eng;

        return $this;
    }

    public function getDescriptionEsp(): ?string
    {
        return $this->description_esp;
    }

    public function setDescriptionEsp(string $description_esp): self
    {
        $this->description_esp = $description_esp;

        return $this;
    }

    public function getDescriptionEng(): ?string
    {
        return $this->description_eng;
    }

    public function setDescriptionEng(string $description_eng): self
    {
        $this->description_eng = $description_eng;

        return $this;
    }

    public function getAge(): ?int
    {
        return $this->age;
    }

    public function setAge(int $age): self
    {
        $this->age = $age;

        return $this;
    }
}
