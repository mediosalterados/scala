<?php

namespace App\Entity;

use App\Repository\NewsRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\DateTime;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;



/**
 * @ORM\Entity(repositoryClass=NewsRepository::class)
 * @ORM\HasLifecycleCallbacks()
 * @Vich\Uploadable
 */
class News
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\ManyToMany(targetEntity=Category::class , inversedBy="new")
     */
    private $categories;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titulo_esp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $titulo_eng;

    /**
     * @ORM\Column(type="text")
     */
    private $contenido_esp;

    /**
     * @ORM\Column(type="text")
     */
    private $contenido_eng;
    
    
    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $banner;

    /**
     * @Vich\UploadableField(mapping="new_images", fileNameProperty="banner")
     * @var File
     */
    private $bannerFile;
    
    
    /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $image;

    /**
     * @Vich\UploadableField(mapping="new_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $updatedAt;
    
    /**
     * @ORM\Column(name="created", type="datetime", nullable=true)
     * @Gedmo\Timestampable(on="create")
     */
    private $created;

    /**
     * @ORM\Column(type="date", nullable=true)
     */
    private $fecha;

    /**
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $destacado;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $podcast;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private $spotify;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     */
    private $description_eng;

    /**
     * @ORM\Column(type="string", length=255 )
     */
    private $url;
    
    
     public function __toString() {
        return $this->name;
    }

    public function __construct()
    {
        $this->categories = new ArrayCollection();
        $this->updatedAt = new \DateTime();
        $this->fecha= new \DateTime();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Category[]
     */
    public function getCategories(): Collection
    {
        return $this->categories;
    }

    public function addCategory(Category $category): self
    {
        if (!$this->categories->contains($category)) {
            $this->categories[] = $category;
            $category->addNew($this);
        }

        return $this;
    }

    public function removeCategory(Category $category): self
    {
        if ($this->categories->removeElement($category)) {
            $this->categories->removeElement($category);
            $category->removeNew($this);
        }

        return $this;
    }

    public function getTituloEsp(): ?string
    {
        return $this->titulo_esp;
    }

    public function setTituloEsp(string $titulo_esp): self
    {
        $this->titulo_esp = $titulo_esp;

        return $this;
    }

    public function getTituloEng(): ?string
    {
        return $this->titulo_eng;
    }

    public function setTituloEng(string $titulo_eng): self
    {
        $this->titulo_eng = $titulo_eng;

        return $this;
    }

    public function getContenidoEsp(): ?string
    {
        return $this->contenido_esp;
    }

    public function setContenidoEsp(string $contenido_esp): self
    {
        $this->contenido_esp = $contenido_esp;

        return $this;
    }

    public function getContenidoEng(): ?string
    {
        return $this->contenido_eng;
    }

    public function setContenidoEng(string $contenido_eng): self
    {
        $this->contenido_eng = $contenido_eng;

        return $this;
    }
    
    
    
     /**
     * @return mixed
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param mixed $imageFile
     */
    public function setImageFile($imageFile): void
    {
        $this->imageFile = $imageFile;

        if ($imageFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }
    
    
     /**
     * @return mixed
     */
    public function getBannerFile()
    {
        return $this->bannerFile;
    }

    /**
     * @param mixed $imageFile
     */
    public function setBannerFile($bannerFile): void
    {
        $this->bannerFile = $bannerFile;

        if ($bannerFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function setBanner($banner)
    {
        $this->banner = $banner;
    }

    public function getBanner()
    {
        return $this->banner;
    }
    
    

    public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdated()
    {
        return $this->updated;
    }
    
        public function getCreated(): ?\DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(\DateTimeInterface $created): self
    {
        $this->created = $created;

        return $this;
    }

    public function getFecha(): ?\DateTimeInterface
    {
        return $this->fecha;
    }

    public function setFecha(?\DateTimeInterface $fecha): self
    {
        $this->fecha = $fecha;

        return $this;
    }

    public function getDestacado(): ?bool
    {
        return $this->destacado;
    }

    public function setDestacado(?bool $destacado): self
    {
        $this->destacado = $destacado;

        return $this;
    }

    public function getPodcast(): ?string
    {
        return $this->podcast;
    }

    public function setPodcast(?string $podcast): self
    {
        $this->podcast = $podcast;

        return $this;
    }

    public function getSpotify(): ?string
    {
        return $this->spotify;
    }

    public function setSpotify(?string $spotify): self
    {
        $this->spotify = $spotify;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getDescriptionEng(): ?string
    {
        return $this->description_eng;
    }

    public function setDescriptionEng(string $description_eng): self
    {
        $this->description_eng = $description_eng;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
    
    
}
