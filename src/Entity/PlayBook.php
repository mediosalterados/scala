<?php

namespace App\Entity;

use App\Repository\PlayBookRepository;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\Validator\Constraints\DateTime;
use Vich\UploaderBundle\Mapping\Annotation as Vich;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=PlayBookRepository::class)
 * @Vich\Uploadable
 */
class PlayBook
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre_esp;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nombre_eng;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $url;
    
      /**
     * @ORM\Column(type="string", length=255 , nullable=true)
     */
    private $image;

    /**
     * @ORM\Column(type="datetime")
     *
     */
    private $updatedAt;
    
    
    /**
     * @Vich\UploadableField(mapping="new_images", fileNameProperty="image")
     * @var File
     */
    private $imageFile;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNombreEsp(): ?string
    {
        return $this->nombre_esp;
    }

    public function setNombreEsp(string $nombre_esp): self
    {
        $this->nombre_esp = $nombre_esp;

        return $this;
    }

    public function getNombreEng(): ?string
    {
        return $this->nombre_eng;
    }

    public function setNombreEng(string $nombre_eng): self
    {
        $this->nombre_eng = $nombre_eng;

        return $this;
    }

    public function getUrl(): ?string
    {
        return $this->url;
    }

    public function setUrl(string $url): self
    {
        $this->url = $url;

        return $this;
    }
    
       /**
     * @return mixed
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @param mixed $imageFile
     */
    public function setImageFile($imageFile): void
    {
        $this->imageFile = $imageFile;

        if ($imageFile) {
            $this->updatedAt = new \DateTime();
        }
    }

    public function setImage($image)
    {
        $this->image = $image;
    }

    public function getImage()
    {
        return $this->image;
    }
       public function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(\DateTimeInterface $updatedAt): self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    public function getUpdated()
    {
        return $this->updated;
    }
    
    
    
}
