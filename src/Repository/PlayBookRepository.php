<?php

namespace App\Repository;

use App\Entity\PlayBook;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method PlayBook|null find($id, $lockMode = null, $lockVersion = null)
 * @method PlayBook|null findOneBy(array $criteria, array $orderBy = null)
 * @method PlayBook[]    findAll()
 * @method PlayBook[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PlayBookRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PlayBook::class);
    }

    // /**
    //  * @return PlayBook[] Returns an array of PlayBook objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('p.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?PlayBook
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
