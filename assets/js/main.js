import $ from 'jquery';

import plyr from 'plyr';
const player = new plyr('#player', {});
var width = $("body").width();

import * as ScrollMagic from 'ScrollMagic';
import "scrollmagic/scrollmagic/minified/plugins/debug.addIndicators.min.js";

window.jQuery = $;
require("@fancyapps/fancybox");

var controller = new ScrollMagic.Controller();

$(document).ready(function () {

    if ($(".start_animation").length) {
        $('.start_animation').addClass('show-scene').delay(1500);
    }

    $('.flags .flag img').hover(function () {

        $(".container-ally").animate({top: $(this).data('top').replace(), left: $(this).data('left').replace()}, 200);
        $(".map").attr('src', $(this).data('img').replace());
        $(".ally").attr('src', $(this).data('ally').replace());
    });
    document.querySelector(".hamburguer").addEventListener("click", function () {
        document.querySelector(".full-menu").classList.toggle("active");
        document.querySelector(".hamburguer").classList.toggle("close-hamburguer");
        $(".color-txt").toggleClass("text-white")
    });
    window.player = player;


    $('.team .close').click(function (event) {
        $('.border-di').removeClass('border-bottom-team');
        $('.collapse-team').slideUp(300);
    });


    if ($(window).width() < 960) {
        $('.single-slider-mobile').slick({
            autoplay: true,
            autoplaySpeed: 4000,
        });
    }



    if ($('.slider-play').length) {
        if (window.matchMedia("(max-width: 768px)").matches) {
            $('.slider-play').slick({
                centerPadding: '40px',
                infinite: true,
                prevArrow: $('.slider-play-prev'),
                nextArrow: $('.slider-play-next'),
                dots: true,

            });
        }
    }

    if ($('.slider-coffee').length) {
        $('.slider-coffee').slick({
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 1,
            centerPadding: '40px',
            infinite: false,
            prevArrow: $('.slider-testimony-prev'),
            nextArrow: $('.slider-testimony-next'),
            dots: true,
            responsive:
                    [
                        {
                            breakpoint: 564,
                            settings: {
                                centerPadding: '10px',
                            }
                        }
                    ]
        });
    }
    if ($('.slider-testimony').length) {
        $('.slider-testimony').slick({
            centerMode: true,
            centerPadding: '0',
            slidesToShow: 3,
            prevArrow: $('.slider-testimony-prev'),
            nextArrow: $('.slider-testimony-next'),
            responsive:
                    [
                        {
                            breakpoint: 564,
                            settings: {
                                infinite: false,
                                centerMode: true,
                                centerPadding: '60px',
                                slidesToShow: 1
                            }
                        }
                    ]
        });
    }


    if ($('.slider-team').length) {
        $('.slider-team').slick({
            centerMode: false,
            centerPadding: '0',
            slidesToShow: 4,
            slidesToScroll: 4,
            infinite: true,
            dots: true,
            prevArrow: $('.slider-team-prev'),
            nextArrow: $('.slider-team-next'),
            responsive:
                    [
                        {
                            breakpoint: 564,
                            settings: {
                                infinite: false,
                                centerMode: false,
                                slidesToShow: 2,
                                slidesToScroll: 2
                            }
                        }
                    ]
        });
    }


    // TEAM COLAPSE
    $('.team .collapse-hover').click(function (event) {
        $('.team .collapse-hover').removeClass('active');
        $(this).addClass('active');
        var teamName = $(this).find('.team-name').text();
        var teamPosition = $(this).find('.team-position').text();
        var teamPhoto = $(this).find('.team-photo').attr('src');
        var teamDescription = $(this).data('description');
        var teamUrl = $(this).data('url');
        console.log(teamUrl);
        if (!$('.collapse-team').is(':visible')) {

            $('.border-di').addClass('border-bottom-team');
            $('.collapse-team-name').text(teamName);
            $('.collapse-team-position').text(teamPosition);
            $('.collapse-team-description').html(teamDescription);
            $('.collapse-team-photo').attr('src', teamPhoto);
            $('.collapse-team-url').attr('href', teamUrl);
            $('.collapse-team-description').html(teamDescription);
            $('.collapse-team').slideDown('300');
        } else {
            $('.collapse-team').fadeOut(250, function () {
                $('.collapse-team-name').text(teamName);
                $('.collapse-team-position').text(teamPosition);
                $('.collapse-team-description').html(teamDescription);
                $('.collapse-team-photo').attr('src', teamPhoto);
                $('.collapse-team-url').attr('href', teamUrl);
                $('.collapse-team-description').html(teamDescription);
                $('.collapse-team').fadeIn(250);
            });
        }
    });

    $('.testimony-btn').click(function (event) {
        $('.testimony-btn').removeClass('active');
        $("html, body").animate({scrollTop: 100}, 500);
        $(this).addClass('active');
        var testimonyPhoto = $(this).find('img').attr('src');
        var testimonyName = $(this).data('name');
        var testimonyAge = $(this).data('age');
        var testimonyTitle = $(this).data('title');
        var testimonyDescription = $(this).data('description');
        $('.testimony-content').fadeOut(250, function () {
            $('.testimony-photo').attr('src', testimonyPhoto);
            $('.testimony-name').text(testimonyName);
            $('.testimony-age').text(testimonyAge);
            $('.testimony-title').text(testimonyTitle);
            $('.testimony-description').html(testimonyDescription);
            $('.testimony-content').fadeIn(250);
        });
    });
});

var scrollIntro = false;
var scrollHigher = false;
var scrollInternational = false;
var scrollNews = false;
var scrollLinkedin = false;
var scrollContact = false;
var scrollFooter = false;
$(window).scroll(function () {
    var scroll = $(window).scrollTop();
    var scrollBottom = $(window).scrollTop() + $(window).innerHeight();
    var scrollPercent = float2int(100 * scroll / ($(document).height() - $(window).height())) + '%';


    if (scroll >= 100) {
        $(".container-menu").addClass("bg-secondary");
        $(".indicator").addClass("down");
    } else {
        $(".container-menu").removeClass("bg-secondary");
        $(".indicator").removeClass("down");
    }

    // ANALYTICS
    if ($('body').hasClass('home')) {
        if (scroll >= $('.ga-intro').offset().top - 400 && scrollIntro == false) {
            scrollIntro = true;
            gtag('event', 'Scroll Home', {'event_category': 'Scroll', 'event_label': 'Introducción (' + scrollPercent + ')'});
        }
        if (scroll >= $('.ga-higher').offset().top + 300 && scrollHigher == false) {
            scrollHigher = true;
            gtag('event', 'Scroll Home', {'event_category': 'Scroll', 'event_label': 'Scala Higher Education (' + scrollPercent + ')'});
        }
        if (scroll >= $('.ga-international').offset().top && scrollInternational == false) {
            scrollInternational = true;
            gtag('event', 'Scroll Home', {'event_category': 'Scroll', 'event_label': 'Internacional (' + scrollPercent + ')'});
        }
        if (scroll >= $('.ga-news').offset().top && scrollNews == false) {
            scrollNews = true;
            gtag('event', 'Scroll Home', {'event_category': 'Scroll', 'event_label': 'Últimas Noticias (' + scrollPercent + ')'});
        }
        if (scroll >= $('.ga-linkedin').offset().top && scrollLinkedin == false) {
            scrollLinkedin = true;
            gtag('event', 'Scroll Home', {'event_category': 'Scroll', 'event_label': 'LinkedIn (' + scrollPercent + ')'});
        }
        if (scroll >= $('.ga-home-contact').offset().top - 200 && scrollContact == false) {
            scrollContact = true;
            gtag('event', 'Scroll Home', {'event_category': 'Scroll', 'event_label': 'Contacto (' + scrollPercent + ')'});
        }
        if (scrollBottom >= $('.ga-home-footer').offset().top + $('.ga-home-footer').innerHeight() && scrollFooter == false) {
            scrollFooter = true;
            gtag('event', 'Scroll Home', {'event_category': 'Scroll', 'event_label': 'Footer (' + scrollPercent + ')'});
        }
    }

    var controller = new ScrollMagic.Controller({globalSceneOptions: {triggerHook: 0.1}});
    if ($(".home_animate").length) {
// home animate
        var scene = new ScrollMagic.Scene({
            triggerElement: "#home-2", offset: -100})
                .reverse(false)
                .setClassToggle(".h-a-4", "show-op")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#home-3", offset: -200})
                .reverse(false)

                .setClassToggle(".h-a-5", "show-op")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#home-3", offset: -150})
                .reverse(false)
                .setClassToggle(".h-a-6", "show-op")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#home-4", offset: -100})
                .reverse(false)

                .setClassToggle(".h-a-7", "show-op")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#home-5", offset: -250})
                .reverse(false)

                .setClassToggle(".h-a-8", "show-op")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#home-6", offset: -300})
                .reverse(false)

                .setClassToggle(".h-a-9", "show-op")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#home-7", offset: -100})
                .reverse(false)

                .setClassToggle(".h-a-10", "show-op")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#home-8", offset: -250})
                .reverse(false)

                .setClassToggle(".h-a-11", "show-op")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#home-9", offset: -350})
                .reverse(false)

                .setClassToggle(".h-a-12", "show-op")
                .addTo(controller);
    }
    if ($(".about_animate").length) {

        var scene = new ScrollMagic.Scene({
            triggerElement: "#about-1", offset: -250})
                .reverse(false)

                .setClassToggle(".a-a-1", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#about-2", offset: -350})
                .reverse(false)

                .setClassToggle(".a-a-2", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#about-3", offset: -250})
                .reverse(false)

                .setClassToggle(".a-a-3", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#about-4", offset: -550})
                .reverse(false)

                .setClassToggle(".a-a-4", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#about-5", offset: -500})
                .reverse(false)

                .setClassToggle(".a-a-5", "show-scene")
                .addTo(controller);
    }
    if ($(".service_animate").length) {
        var scene = new ScrollMagic.Scene({
            triggerElement: "#service-1", offset: -150})
                .reverse(false)

                .setClassToggle(".a-s-1", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#service-2", offset: -155})
                .reverse(false)

                .setClassToggle(".a-s-2", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#service-3", offset: -155})
                .reverse(false)

                .setClassToggle(".a-s-3", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#service-4", offset: -155})
                .reverse(false)

                .setClassToggle(".a-s-4", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#service-5", offset: -155})
                .reverse(false)

                .setClassToggle(".a-s-5", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#service-6", offset: -155})
                .reverse(false)

                .setClassToggle(".a-s-6", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#service-7", offset: -250})
                .reverse(false)

                .setClassToggle(".a-s-7", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#service-8", offset: -200})
                .reverse(false)

                .setClassToggle(".a-s-8", "show-scene")
                .addTo(controller);
    }
    if ($(".allies_animate").length) {

        var scene = new ScrollMagic.Scene({
            triggerElement: "#allies-1", offset: -150})
                .reverse(false)

                .setClassToggle(".a-al-1", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#allies-2", offset: -100})
                .reverse(false)

                .setClassToggle(".a-al-2", "show-scene")
                .addTo(controller);
    }
    if ($(".blog_animate").length) {

        var scene = new ScrollMagic.Scene({
            triggerElement: "#blog-1", offset: -150})
                .reverse(false)

                .setClassToggle(".a-b-1", "show-op")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#blog-2", offset: -50})
                .reverse(false)

                .setClassToggle(".a-b-2", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#blog-3", offset: -50})
                .reverse(false)

                .setClassToggle(".a-b-3", "show-scene")
                .addTo(controller);
        new ScrollMagic.Scene({
            triggerElement: "#blog-4", offset: -50})
                .reverse(false)
                .setClassToggle(".a-b-4", "show-op")
                .addTo(controller);
    }


});

function float2int(value) {
    return value | 0;
}
